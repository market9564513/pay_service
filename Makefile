CURRENT_DIR=$(shell pwd)

proto-gen:
	sudo rm -rf ${GOROOT}/src/genproto
	./scripts/gen-proto.sh  ${CURRENT_DIR}

swag_init:
	swag init -g api/router.go -o api/docs

run:
	go run cmd/main.go
