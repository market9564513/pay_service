alter table if exists shifts alter column start_hour type time,
                               alter column end_hour type time;
