alter table if exists shifts alter column start_hour type timestamp,
                             alter column end_hour type timestamp;
