drop table if exists sale_products;

drop table if exists sales;

drop table if exists shifts;

drop table if exists couriers;

drop table if exists staffs;

drop table if exists sales_points;

drop table if exists branches;