package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	pbp "pay_service/genproto/pay_service"
	"pay_service/grpc/client"
	"pay_service/pkg/logger"
	"pay_service/service"
	"pay_service/storage"
)

func SetUpServer(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	pbp.RegisterBranchServiceServer(grpcServer, service.NewBranchService(storage, services, log))
	pbp.RegisterSalesPointServiceServer(grpcServer, service.NewSalePointService(storage, services, log))
	pbp.RegisterStaffServiceServer(grpcServer, service.NewStaffService(storage, services, log))
	pbp.RegisterCourierServiceServer(grpcServer, service.NewCourierService(storage, services, log))
	pbp.RegisterShiftServiceServer(grpcServer, service.NewShiftService(storage, services, log))
	pbp.RegisterSaleServiceServer(grpcServer, service.NewSaleService(storage, services, log))
	pbp.RegisterSaleProductServiceServer(grpcServer, service.NewSaleProductService(storage, services, log))
	pbp.RegisterAuthServiceServer(grpcServer, service.NewAuthService(storage, services, log))

	reflection.Register(grpcServer)

	return grpcServer
}
