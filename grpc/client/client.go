package client

import (
	"google.golang.org/grpc"
	"pay_service/config"
	pbp "pay_service/genproto/pay_service"
	pbr "pay_service/genproto/repository_service"
	"pay_service/pkg/logger"
)

type IServiceManager interface {
	// pay_service
	BranchService() pbp.BranchServiceClient
	SalePointService() pbp.SalesPointServiceClient
	StaffService() pbp.StaffServiceClient
	CourierService() pbp.CourierServiceClient
	ShiftService() pbp.ShiftServiceClient
	SaleService() pbp.SaleServiceClient
	SaleProductService() pbp.SaleProductServiceClient
	AuthService() pbp.AuthServiceClient

	// repository_service
	CategoryService() pbr.CategoryServiceClient
	ProductService() pbr.ProductServiceClient
	IncomeService() pbr.IncomeServiceClient
	IncomeProductService() pbr.IncomeProductServiceClient
	StorageService() pbr.StorageServiceClient
}

type grpcClient struct {
	// pay_service
	branchService      pbp.BranchServiceClient
	salePointService   pbp.SalesPointServiceClient
	staffService       pbp.StaffServiceClient
	courierService     pbp.CourierServiceClient
	shiftService       pbp.ShiftServiceClient
	saleService        pbp.SaleServiceClient
	saleProductService pbp.SaleProductServiceClient
	authService        pbp.AuthServiceClient

	// repository_service
	categoryService      pbr.CategoryServiceClient
	productService       pbr.ProductServiceClient
	incomeService        pbr.IncomeServiceClient
	incomeProductService pbr.IncomeProductServiceClient
	storageService       pbr.StorageServiceClient
}

func NewGrpcClient(cfg config.Config, log logger.ILogger) (IServiceManager, error) {
	connPayService, err := grpc.Dial(cfg.PayGrpcServiceHost+cfg.PayGrpcServicePort, grpc.WithInsecure())
	if err != nil {
		log.Error("error is while connecting to pay service", logger.Error(err))
		return nil, err
	}

	connRepositoryService, err := grpc.Dial(cfg.RepositoryGrpcServiceHost+cfg.RepositoryGrpcServicePort, grpc.WithInsecure())
	if err != nil {
		log.Error("error is while connecting to repo service", logger.Error(err))
		return nil, err
	}

	return grpcClient{
		// pay_service
		branchService:      pbp.NewBranchServiceClient(connPayService),
		salePointService:   pbp.NewSalesPointServiceClient(connPayService),
		staffService:       pbp.NewStaffServiceClient(connPayService),
		courierService:     pbp.NewCourierServiceClient(connPayService),
		shiftService:       pbp.NewShiftServiceClient(connPayService),
		saleService:        pbp.NewSaleServiceClient(connPayService),
		saleProductService: pbp.NewSaleProductServiceClient(connPayService),
		authService:        pbp.NewAuthServiceClient(connPayService),

		// repository_service
		categoryService:      pbr.NewCategoryServiceClient(connRepositoryService),
		productService:       pbr.NewProductServiceClient(connRepositoryService),
		incomeService:        pbr.NewIncomeServiceClient(connRepositoryService),
		incomeProductService: pbr.NewIncomeProductServiceClient(connRepositoryService),
		storageService:       pbr.NewStorageServiceClient(connRepositoryService),
	}, nil
}

// pay_service
func (g grpcClient) BranchService() pbp.BranchServiceClient {
	return g.branchService
}

func (g grpcClient) SalePointService() pbp.SalesPointServiceClient {
	return g.salePointService
}

func (g grpcClient) StaffService() pbp.StaffServiceClient {
	return g.staffService
}

func (g grpcClient) CourierService() pbp.CourierServiceClient {
	return g.courierService
}

func (g grpcClient) ShiftService() pbp.ShiftServiceClient {
	return g.shiftService
}

func (g grpcClient) SaleService() pbp.SaleServiceClient {
	return g.saleService
}

func (g grpcClient) SaleProductService() pbp.SaleProductServiceClient {
	return g.saleProductService
}

func (g grpcClient) AuthService() pbp.AuthServiceClient {
	return g.authService
}

// repository_service
func (g grpcClient) CategoryService() pbr.CategoryServiceClient {
	return g.categoryService
}

func (g grpcClient) ProductService() pbr.ProductServiceClient {
	return g.productService
}

func (g grpcClient) IncomeService() pbr.IncomeServiceClient {
	return g.incomeService
}

func (g grpcClient) IncomeProductService() pbr.IncomeProductServiceClient {
	return g.incomeProductService
}

func (g grpcClient) StorageService() pbr.StorageServiceClient {
	return g.storageService
}
