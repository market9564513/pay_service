package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"net"
	"pay_service/config"
	"pay_service/grpc"
	"pay_service/grpc/client"
	"pay_service/pkg/logger"
	"pay_service/storage/postgres"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	pgStore, err := postgres.New(context.Background(), cfg, log)
	if err != nil {
		log.Error("error is while connecting to db", logger.Error(err))
	}

	services, err := client.NewGrpcClient(cfg, log)
	if err != nil {
		log.Error("error is while initializing grpc client", logger.Error(err))
		return
	}

	grpcServer := grpc.SetUpServer(pgStore, services, log)

	lis, err := net.Listen("tcp", cfg.PayGrpcServiceHost+cfg.PayGrpcServicePort)
	if err != nil {
		log.Error("error is while listening", logger.Error(err))
		return
	}

	log.Info("Service running....", logger.String("port", cfg.PayGrpcServicePort))
	if err := grpcServer.Serve(lis); err != nil {
		log.Error("error is while serving list", logger.Error(err))
	}
}
