package generate

import (
	"fmt"
	"log"
	"strconv"
	"time"
)

var number = 1

func PointIDGenerate(name, salesPointID string) string {
	var id string
	letter := name[:2]

	if salesPointID == "" {
		numStr := fmt.Sprintf("%04d", number)
		id = letter + "-" + numStr

		return id
	}

	idOrder := salesPointID[3:]

	num, _ := strconv.Atoi(idOrder)
	num++

	numStr := fmt.Sprintf("%04d", num)

	id = letter + "-" + numStr

	return id
}

func SaleIDGenerate(lastID string) string {
	var id string

	if lastID == "" {
		currentTime := time.Now()
		id = fmt.Sprintf("%d-%d-%d-%d", currentTime.Year(), currentTime.Month(), currentTime.Day(), 1)
		return id
	}

	idStr := lastID[9:]
	idInt, err := strconv.Atoi(idStr)
	if err != nil {
		log.Println("error is while converting sale id", err)
	}

	idInt++

	checkDay := lastID[:8]
	if checkDay != time.DateOnly {
		idInt = 1
	}

	currentTime := time.Now()
	id = fmt.Sprintf("%d-%d-%d-%d", currentTime.Year(), currentTime.Month(), currentTime.Day(), idInt)

	return id
}

func ShiftIDGenerate(lastID string) string {
	var (
		randNum, incomeID string
	)

	if lastID == "" {
		randNum = fmt.Sprintf("%04d", number)
		incomeID = "C-" + randNum
		return incomeID
	}

	numberPart := lastID[2:]
	fmt.Println("num", numberPart)

	num, err := strconv.Atoi(numberPart)
	if err != nil {
		log.Println("error is while converting income id to int", err)
	}

	num++
	randNum = fmt.Sprintf("%04d", num)
	incomeID = "C-" + randNum

	return incomeID
}
