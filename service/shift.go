package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbp "pay_service/genproto/pay_service"
	"pay_service/grpc/client"
	"pay_service/pkg/logger"
	"pay_service/storage"
)

type shiftService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pbp.UnimplementedShiftServiceServer
}

func NewShiftService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *shiftService {
	return &shiftService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *shiftService) Create(ctx context.Context, shift *pbp.CreateShift) (*pbp.Shift, error) {
	return s.storage.Shift().Create(ctx, shift)
}

func (s *shiftService) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Shift, error) {
	return s.storage.Shift().Get(ctx, key)
}

func (s *shiftService) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.ShiftsResponse, error) {
	return s.storage.Shift().GetList(ctx, request)
}

func (s *shiftService) Update(ctx context.Context, shift *pbp.Shift) (*pbp.Shift, error) {
	return s.storage.Shift().Update(ctx, shift)
}

func (s *shiftService) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	return s.storage.Shift().Delete(ctx, key)
}

func (s *shiftService) UpdateStatus(ctx context.Context, shift *pbp.Shift) (*pbp.Msg, error) {
	shiftData, err := s.storage.Shift().Get(ctx, &pbp.PrimaryKey{Id: shift.Id})
	if err != nil {
		s.log.Error("error is while getting shift data", logger.Error(err))
		return nil, err
	}

	if shiftData.Status == shift.Status { // opened == opened
		return &pbp.Msg{Message: "your shift not closed, close before open it"}, nil
	}

	message, err := s.storage.Shift().UpdateStatus(ctx, shift)
	if err != nil {
		s.log.Error("error is while updating shift status", logger.Error(err))
		return nil, err
	}

	return message, nil
}
