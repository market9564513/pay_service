package service

import (
	context "context"
	"github.com/golang/protobuf/ptypes/empty"
	pbp "pay_service/genproto/pay_service"
	"pay_service/grpc/client"
	"pay_service/pkg/logger"
	"pay_service/storage"
)

type staffService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pbp.UnimplementedStaffServiceServer
}

func NewStaffService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *staffService {
	return &staffService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *staffService) Create(ctx context.Context, staff *pbp.CreateStaff) (*pbp.Staff, error) {
	return s.storage.Staff().Create(ctx, staff)
}

func (s *staffService) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Staff, error) {
	return s.storage.Staff().Get(ctx, key)
}

func (s *staffService) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.StaffsResponse, error) {
	return s.storage.Staff().GetList(ctx, request)
}

func (s *staffService) Update(ctx context.Context, staff *pbp.Staff) (*pbp.Staff, error) {
	return s.storage.Staff().Update(ctx, staff)
}

func (s *staffService) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	return s.storage.Staff().Delete(ctx, key)
}
