package service

import (
	"context"
	"errors"
	"github.com/golang/protobuf/ptypes/empty"
	pbp "pay_service/genproto/pay_service"
	pbr "pay_service/genproto/repository_service"
	"pay_service/grpc/client"
	"pay_service/pkg/logger"
	"pay_service/storage"
)

type saleProductService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pbp.UnimplementedSaleProductServiceServer
}

func NewSaleProductService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *saleProductService {
	return &saleProductService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *saleProductService) Create(ctx context.Context, product *pbp.CreateSaleProduct) (*pbp.SaleProduct, error) {
	productData, err := s.services.ProductService().Get(ctx, &pbr.PrimaryKey{Id: product.ProductId})
	if err != nil {
		s.log.Error("error is while getting product date", logger.Error(err))
		return nil, err
	}

	storageData, err := s.services.StorageService().GetList(ctx, &pbr.GetRequest{
		Page:  1,
		Limit: 10,
	})

	productCanSold := make(map[string]int32)
	for _, products := range storageData.GetStorages() {
		if products.ProductId == product.ProductId && products.Quantity >= product.Quantity {
			productCanSold[products.ProductId] = products.Quantity
		} else {
			return nil, errors.New("in storage not enough product")
		}
	}

	product.Price = productData.Price
	product.ExistProductQuantity = productCanSold[product.ProductId]

	product.TotalPrice = (product.Price + 20*product.Price/100) * product.Quantity
	sale, err := s.storage.SaleProduct().Create(ctx, product)
	if err != nil {
		s.log.Error("error in service layer while creating sale", logger.Error(err))
	}

	return sale, nil
}

func (s *saleProductService) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.SaleProduct, error) {
	return s.storage.SaleProduct().Get(ctx, key)
}

func (s *saleProductService) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.SaleProductsResponse, error) {
	return s.storage.SaleProduct().GetList(ctx, request)
}

func (s *saleProductService) Update(ctx context.Context, product *pbp.SaleProduct) (*pbp.SaleProduct, error) {
	return s.storage.SaleProduct().Update(ctx, product)
}

func (s *saleProductService) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	return s.storage.SaleProduct().Delete(ctx, key)
}
