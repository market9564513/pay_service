package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbp "pay_service/genproto/pay_service"
	"pay_service/grpc/client"
	"pay_service/pkg/logger"
	"pay_service/storage"
)

type salePointService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pbp.UnimplementedSalesPointServiceServer
}

func NewSalePointService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *salePointService {
	return &salePointService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *salePointService) Create(ctx context.Context, request *pbp.CreateSalesPoint) (*pbp.SalesPoint, error) {
	salePoint, err := s.storage.SalePoint().Create(ctx, request)
	if err != nil {
		s.log.Error("error in service layer while creating sale point", logger.Error(err))
		return nil, err
	}

	return salePoint, nil
}

func (s *salePointService) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.SalesPoint, error) {
	return s.storage.SalePoint().Get(ctx, key)
}

func (s *salePointService) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.SalesPointsResponse, error) {
	return s.storage.SalePoint().GetList(ctx, request)
}

func (s *salePointService) Update(ctx context.Context, point *pbp.SalesPoint) (*pbp.SalesPoint, error) {
	return s.storage.SalePoint().Update(ctx, point)
}

func (s *salePointService) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	return s.storage.SalePoint().Delete(ctx, key)
}
