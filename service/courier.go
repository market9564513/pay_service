package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbp "pay_service/genproto/pay_service"
	"pay_service/grpc/client"
	"pay_service/pkg/logger"
	"pay_service/storage"
)

type courierService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pbp.UnimplementedCourierServiceServer
}

func NewCourierService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *courierService {
	return &courierService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (c *courierService) Create(ctx context.Context, courier *pbp.CreateCourier) (*pbp.Courier, error) {
	return c.storage.Courier().Create(ctx, courier)
}

func (c *courierService) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Courier, error) {
	return c.storage.Courier().Get(ctx, key)
}

func (c *courierService) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.CouriersResponse, error) {
	return c.storage.Courier().GetList(ctx, request)
}

func (c *courierService) Update(ctx context.Context, courier *pbp.Courier) (*pbp.Courier, error) {
	return c.storage.Courier().Update(ctx, courier)
}

func (c *courierService) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	return c.storage.Courier().Delete(ctx, key)
}
