package service

import (
	context "context"
	"github.com/golang/protobuf/ptypes/empty"
	pbp "pay_service/genproto/pay_service"
	"pay_service/grpc/client"
	"pay_service/pkg/logger"
	"pay_service/storage"
)

type branchService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pbp.UnimplementedBranchServiceServer
}

func NewBranchService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *branchService {
	return &branchService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (b *branchService) Create(ctx context.Context, branch *pbp.Branch) (*pbp.Branch, error) {
	return b.storage.Branch().Create(ctx, branch)
}

func (b *branchService) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Branch, error) {
	return b.storage.Branch().Get(ctx, key)
}

func (b *branchService) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.BranchesResponse, error) {
	return b.storage.Branch().GetList(ctx, request)
}

func (b *branchService) Update(ctx context.Context, branch *pbp.Branch) (*pbp.Branch, error) {
	return b.storage.Branch().Update(ctx, branch)
}

func (b *branchService) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	return b.storage.Branch().Delete(ctx, key)
}
