package service

import (
	"context"
	pbp "pay_service/genproto/pay_service"
	"pay_service/grpc/client"
	"pay_service/pkg/logger"
	"pay_service/pkg/security"
	"pay_service/storage"
)

type authService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pbp.UnimplementedAuthServiceServer
}

func NewAuthService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *authService {
	return &authService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (a *authService) Login(ctx context.Context, request *pbp.LoginRequest) (*pbp.LoginResponse, error) {
	resp, err := a.storage.Staff().GetByCredentials(ctx, request)
	if err != nil {
		a.log.Error("error is while getting credentials in service layer", logger.Error(err))
		return nil, err
	}

	if !security.CompareHashAndPassword(resp.Password, request.Password) {
		a.log.Error("invalid password", logger.Error(err))
		return nil, err
	}

	return &pbp.LoginResponse{
		Id:       resp.GetId(),
		Phone:    resp.GetPhone(),
		UserRole: resp.GetUserRole(),
	}, nil
}
