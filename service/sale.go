package service

import (
	"context"
	"errors"
	"github.com/golang/protobuf/ptypes/empty"
	pbp "pay_service/genproto/pay_service"
	pbr "pay_service/genproto/repository_service"
	"pay_service/grpc/client"
	"pay_service/pkg/logger"
	"pay_service/storage"
)

type saleService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pbp.UnimplementedSaleServiceServer
}

func NewSaleService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *saleService {
	return &saleService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (s *saleService) Create(ctx context.Context, sale *pbp.CreateSale) (*pbp.Sale, error) {
	shiftData, err := s.storage.Shift().Get(ctx, &pbp.PrimaryKey{Id: sale.ShiftId})
	if err != nil {
		s.log.Error("error in service layer while getting shift data", logger.Error(err))
		return nil, err
	}

	if shiftData.Status == "closed" {
		return nil, errors.New("you cannot sale because opened shift does not exist")
	}

	createdSale, err := s.storage.Sale().Create(ctx, sale)
	if err != nil {
		s.log.Error("error in service layer while creating sale", logger.Error(err))
		return nil, nil
	}

	return createdSale, nil
}

func (s *saleService) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Sale, error) {
	return s.storage.Sale().Get(ctx, key)
}

func (s *saleService) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.SalesResponse, error) {
	return s.storage.Sale().GetList(ctx, request)
}

func (s *saleService) Update(ctx context.Context, sale *pbp.Sale) (*pbp.Sale, error) {
	return s.storage.Sale().Update(ctx, sale)
}

func (s *saleService) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	return s.storage.Sale().Delete(ctx, key)
}

func (s *saleService) EndSale(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Sale, error) {
	soldProducts, err := s.storage.SaleProduct().GetList(ctx, &pbp.GetRequest{
		Page:   1,
		Limit:  10,
		Search: key.Id,
	})
	if err != nil {
		s.log.Error("error in service layer while getting list of sold saleProducts", logger.Error(err))
		return nil, err
	}

	storageData, err := s.services.StorageService().GetList(ctx, &pbr.GetRequest{
		Page:  1,
		Limit: 10,
	})

	saleProducts := make(map[string]*pbp.SaleProduct)
	for _, p := range soldProducts.GetSaleProducts() {
		saleProducts[p.ProductId] = p
	}

	for _, str := range storageData.GetStorages() {
		if _, exists := saleProducts[str.ProductId]; exists {
			_, err = s.services.StorageService().Update(ctx, &pbr.Storage{
				Id:         str.Id,
				Quantity:   str.Quantity - saleProducts[str.ProductId].Quantity,
				TotalPrice: str.TotalPrice - (saleProducts[str.ProductId].Price * saleProducts[str.ProductId].Quantity),
			})
			if err != nil {
				s.log.Error("error is while updating sold saleProducts", logger.Error(err))
				return nil, err
			}
		}
	}

	//	_, err = str.services.StorageService().MultipleUpdateSoldProducts(ctx,
	//		&pbr.MultipleUpdateRequest{Products: saleProducts})

	sale, err := s.storage.Sale().Update(ctx, &pbp.Sale{
		Id:     key.Id,
		Status: "finished",
	})
	if err != nil {
		s.log.Error("error is while finishing sale", logger.Error(err))
		return nil, err
	}

	return sale, nil
}
