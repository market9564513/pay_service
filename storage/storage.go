package storage

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbp "pay_service/genproto/pay_service"
)

type IStorage interface {
	Close()
	Branch() IBranchStorage
	SalePoint() ISalePointStorage
	Staff() IStaffStorage
	Courier() ICourierStorage
	Shift() IShiftStorage
	Sale() ISaleStorage
	SaleProduct() ISaleProductStorage
}

type IBranchStorage interface {
	Create(context.Context, *pbp.Branch) (*pbp.Branch, error)
	Get(context.Context, *pbp.PrimaryKey) (*pbp.Branch, error)
	GetList(context.Context, *pbp.GetRequest) (*pbp.BranchesResponse, error)
	Update(context.Context, *pbp.Branch) (*pbp.Branch, error)
	Delete(context.Context, *pbp.PrimaryKey) (*empty.Empty, error)
}

type ISalePointStorage interface {
	Create(context.Context, *pbp.CreateSalesPoint) (*pbp.SalesPoint, error)
	Get(context.Context, *pbp.PrimaryKey) (*pbp.SalesPoint, error)
	GetList(context.Context, *pbp.GetRequest) (*pbp.SalesPointsResponse, error)
	Update(context.Context, *pbp.SalesPoint) (*pbp.SalesPoint, error)
	Delete(context.Context, *pbp.PrimaryKey) (*empty.Empty, error)
}

type IStaffStorage interface {
	Create(context.Context, *pbp.CreateStaff) (*pbp.Staff, error)
	Get(context.Context, *pbp.PrimaryKey) (*pbp.Staff, error)
	GetList(context.Context, *pbp.GetRequest) (*pbp.StaffsResponse, error)
	Update(context.Context, *pbp.Staff) (*pbp.Staff, error)
	Delete(context.Context, *pbp.PrimaryKey) (*empty.Empty, error)
	GetByCredentials(context.Context, *pbp.LoginRequest) (*pbp.Staff, error)
}

type ICourierStorage interface {
	Create(context.Context, *pbp.CreateCourier) (*pbp.Courier, error)
	Get(context.Context, *pbp.PrimaryKey) (*pbp.Courier, error)
	GetList(context.Context, *pbp.GetRequest) (*pbp.CouriersResponse, error)
	Update(context.Context, *pbp.Courier) (*pbp.Courier, error)
	Delete(context.Context, *pbp.PrimaryKey) (*empty.Empty, error)
}

type IShiftStorage interface {
	Create(context.Context, *pbp.CreateShift) (*pbp.Shift, error)
	Get(context.Context, *pbp.PrimaryKey) (*pbp.Shift, error)
	GetList(context.Context, *pbp.GetRequest) (*pbp.ShiftsResponse, error)
	Update(context.Context, *pbp.Shift) (*pbp.Shift, error)
	Delete(context.Context, *pbp.PrimaryKey) (*empty.Empty, error)
	UpdateStatus(context.Context, *pbp.Shift) (*pbp.Msg, error)
}

type ISaleStorage interface {
	Create(context.Context, *pbp.CreateSale) (*pbp.Sale, error)
	Get(context.Context, *pbp.PrimaryKey) (*pbp.Sale, error)
	GetList(context.Context, *pbp.GetRequest) (*pbp.SalesResponse, error)
	Update(context.Context, *pbp.Sale) (*pbp.Sale, error)
	Delete(context.Context, *pbp.PrimaryKey) (*empty.Empty, error)
}

type ISaleProductStorage interface {
	Create(context.Context, *pbp.CreateSaleProduct) (*pbp.SaleProduct, error)
	Get(context.Context, *pbp.PrimaryKey) (*pbp.SaleProduct, error)
	GetList(context.Context, *pbp.GetRequest) (*pbp.SaleProductsResponse, error)
	Update(context.Context, *pbp.SaleProduct) (*pbp.SaleProduct, error)
	Delete(context.Context, *pbp.PrimaryKey) (*empty.Empty, error)
}
