package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jackc/pgx/v5/pgxpool"
	pbp "pay_service/genproto/pay_service"
	"pay_service/pkg/generate"
	"pay_service/pkg/helper"
	"pay_service/pkg/logger"
)

type shiftRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewShiftRepo(db *pgxpool.Pool, log logger.ILogger) *shiftRepo {
	return &shiftRepo{
		db:  db,
		log: log,
	}
}

func (s *shiftRepo) Create(ctx context.Context, request *pbp.CreateShift) (*pbp.Shift, error) {
	var (
		lastID, id  string
		response    = pbp.Shift{}
		lastIDQuery = `select id from shifts order by id desc limit 1`
	)

	if err := s.db.QueryRow(ctx, lastIDQuery).Scan(&lastID); err != nil {
		id = generate.ShiftIDGenerate("")
	} else {
		id = generate.ShiftIDGenerate(lastID)
	}

	searchingColumn := fmt.Sprintf("%s %s", request.GetStaffId(), request.GetSalePointId())

	query := `insert into shifts (id, staff_id, sale_point_id, start_hour, end_hour, status, searching_column) 
                    values ($1, $2, $3, $4, $5, $6, $7)
                    returning id, staff_id, sale_point_id, start_hour::text, end_hour::text, status, created_at::text`
	if err := s.db.QueryRow(ctx, query,
		id,
		request.GetStaffId(),
		request.GetSalePointId(),
		request.GetStartHour(),
		request.GetEndHour(),
		request.GetStatus(),
		searchingColumn).
		Scan(
			&response.Id,
			&response.StaffId,
			&response.SalePointId,
			&response.StartHour,
			&response.EndHour,
			&response.Status,
			&response.CreatedAt,
		); err != nil {
		s.log.Error("error is while creating shift", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *shiftRepo) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Shift, error) {
	response := pbp.Shift{}
	query := `select id, staff_id, sale_point_id, start_hour::text, end_hour::text, status, created_at::text, updated_at::text from shifts where id = $1 and deleted_at = 0`
	if err := s.db.QueryRow(ctx, query, key.GetId()).Scan(
		&response.Id,
		&response.StaffId,
		&response.SalePointId,
		&response.StartHour,
		&response.EndHour,
		&response.Status,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		s.log.Error("error is while getting by id", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *shiftRepo) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.ShiftsResponse, error) {
	var (
		offset                                = (request.GetPage() - 1) * request.GetLimit()
		resp                                  pbp.ShiftsResponse
		filter, pagination, query, countQuery string
		count                                 = int32(0)
	)
	pagination = ` OFFSET $1 LIMIT $2`
	filter = ` where deleted_at = 0 `
	if request.GetSearch() != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%'", request.GetSearch())
	}

	countQuery = `select count(1) from shifts ` + filter
	if err := s.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		s.log.Error("error is while scanning count", logger.Error(err))
		return nil, err
	}

	query = `select id, staff_id, sale_point_id, start_hour::text, end_hour::text, status, created_at::text, updated_at::text from shifts ` + filter + pagination
	rows, err := s.db.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		s.log.Error("error is while selecting all from sales", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		shift := pbp.Shift{}
		if err := rows.Scan(
			&shift.Id,
			&shift.StaffId,
			&shift.SalePointId,
			&shift.StartHour,
			&shift.EndHour,
			&shift.Status,
			&shift.CreatedAt,
			&shift.UpdatedAt,
		); err != nil {
			s.log.Error("error is while scanning all in sale", logger.Error(err))
			return nil, err
		}
		resp.Shifts = append(resp.Shifts, &shift)
	}
	resp.Count = count
	return &resp, nil
}

func (s *shiftRepo) Update(ctx context.Context, request *pbp.Shift) (*pbp.Shift, error) {
	var (
		params   = make(map[string]interface{})
		response = pbp.Shift{}
		query    = `update shifts set `
		filter   = ""
	)

	params["id"] = request.GetId()
	fmt.Println("courier id", request.GetId())

	if request.GetStaffId() != "" {
		params["staff_id"] = request.GetStaffId()

		filter += " staff_id = @staff_id,"
	}

	if request.GetSalePointId() != "" {
		params["sale_point_id"] = request.GetSalePointId()

		filter += " sale_point_id = @sale_point_id,"
	}

	if request.GetStartHour() != "" {
		params["start_hour"] = request.GetStartHour()

		filter += " start_hour = @start_hour,"
	}

	if request.GetEndHour() != "" {
		params["end_hour"] = request.GetEndHour()

		filter += " end_hour = @end_hour,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, staff_id, sale_point_id, start_hour::text, end_hour::text, status, created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := s.db.QueryRow(ctx, fullQuery, args...).Scan(
		&response.Id,
		&response.StaffId,
		&response.SalePointId,
		&response.StartHour,
		&response.EndHour,
		&response.Status,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		s.log.Error("error is while scanning updated fields from shift", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *shiftRepo) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	query := `update shifts set deleted_at = extract(epoch from current_timestamp) where id = $1`
	rowsAffected, err := s.db.Exec(ctx, query, key.GetId())
	if err != nil {
		s.log.Error("error is while deleting", logger.Error(err))
		return nil, err
	}

	if n := rowsAffected.RowsAffected(); n == 0 {
		s.log.Error("error is while rows affected", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *shiftRepo) UpdateStatus(ctx context.Context, request *pbp.Shift) (*pbp.Msg, error) {
	query := `update shifts set status = $1 where id = $2`
	rowsAffected, err := s.db.Exec(ctx, query, request.GetStatus(), request.GetId())
	if err != nil {
		s.log.Error("error is while updating status", logger.Error(err))
		return nil, err
	}

	if n := rowsAffected.RowsAffected(); n == 0 {
		s.log.Error("error is while rows affected", logger.Error(err))
		return nil, err
	}

	var message string
	if request.Status == "opened" {
		message = "shift opened"
	} else {
		message = "shift closed"
	}

	return &pbp.Msg{Message: message}, nil
}
