package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jackc/pgx/v5/pgxpool"
	pbp "pay_service/genproto/pay_service"
	"pay_service/pkg/generate"
	"pay_service/pkg/helper"
	"pay_service/pkg/logger"
)

type saleRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewSaleRepo(db *pgxpool.Pool, log logger.ILogger) *saleRepo {
	return &saleRepo{
		db:  db,
		log: log,
	}
}

func (s *saleRepo) Create(ctx context.Context, request *pbp.CreateSale) (*pbp.Sale, error) {
	var (
		lastID, id  string
		response    = pbp.Sale{}
		lastIDQuery = `select id from sales order by id desc limit 1`
		barcode     = generate.BarcodeGenerate()
		createdAt   sql.NullString
	)

	if err := s.db.QueryRow(ctx, lastIDQuery).Scan(&lastID); err != nil {
		id = generate.SaleIDGenerate("")
	} else {
		id = generate.SaleIDGenerate(lastID)
	}
	fmt.Println("las", lastID)
	fmt.Println("id", id)
	searchingColumn := fmt.Sprintf("%s %s %s %s", barcode, request.GetShiftId(), request.GetSalePointId(), request.GetStaffId())

	query := `insert into sales (id, barcode, shift_id, sale_point_id, staff_id, status, payment, searching_column) 
                    values ($1, $2, $3, $4, $5, $6, $7, $8)
                    returning id, barcode, shift_id, sale_point_id, staff_id, status, payment, created_at::text`
	if err := s.db.QueryRow(ctx, query,
		id,
		barcode,
		request.GetShiftId(),
		request.GetSalePointId(),
		request.GetStaffId(),
		request.GetStatus(),
		request.GetPayment(),
		searchingColumn).
		Scan(
			&response.Id,
			&response.Barcode,
			&response.ShiftId,
			&response.SalePointId,
			&response.StaffId,
			&response.Status,
			&response.Payment,
			&createdAt,
		); err != nil {
		s.log.Error("error is while creating sale", logger.Error(err))
		return nil, err
	}

	if createdAt.Valid {
		response.CreatedAt = createdAt.String
	}

	return &response, nil
}

func (s *saleRepo) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Sale, error) {
	var (
		response             = pbp.Sale{}
		createdAt, updatedAt = sql.NullString{}, sql.NullString{}
	)

	query := `select id, barcode, shift_id, sale_point_id, staff_id, status, payment, created_at::text, updated_at::text from sales where id = $1 and deleted_at = 0`
	if err := s.db.QueryRow(ctx, query, key.GetId()).Scan(
		&response.Id,
		&response.Barcode,
		&response.ShiftId,
		&response.SalePointId,
		&response.StaffId,
		&response.Status,
		&response.Payment,
		&createdAt,
		&updatedAt,
	); err != nil {
		s.log.Error("error is while getting by id", logger.Error(err))
		return nil, err
	}

	if createdAt.Valid {
		response.CreatedAt = createdAt.String
	}

	if updatedAt.Valid {
		response.UpdatedAt = updatedAt.String
	}

	return &response, nil
}

func (s *saleRepo) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.SalesResponse, error) {
	var (
		offset                                = (request.GetPage() - 1) * request.GetLimit()
		resp                                  pbp.SalesResponse
		filter, pagination, query, countQuery string
		count                                 = int32(0)
		createdAt, updatedAt                  = sql.NullString{}, sql.NullString{}
	)
	pagination = ` OFFSET $1 LIMIT $2`

	filter = ` where deleted_at = 0 `

	if request.GetSearch() != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%'", request.GetSearch())
	}

	countQuery = `select count(1) from sales ` + filter
	if err := s.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		s.log.Error("error is while scanning count", logger.Error(err))
		return nil, err
	}

	query = `select id, barcode, shift_id, sale_point_id, staff_id, status, payment, created_at::text, updated_at::text from sales ` + filter + pagination
	rows, err := s.db.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		s.log.Error("error is while selecting all from sales", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		sale := pbp.Sale{}
		if err := rows.Scan(
			&sale.Id,
			&sale.Barcode,
			&sale.ShiftId,
			&sale.SalePointId,
			&sale.StaffId,
			&sale.Status,
			&sale.Payment,
			&createdAt,
			&updatedAt,
		); err != nil {
			s.log.Error("error is while scanning all in sale", logger.Error(err))
			return nil, err
		}
		if createdAt.Valid {
			sale.CreatedAt = createdAt.String
		}

		if updatedAt.Valid {
			sale.UpdatedAt = updatedAt.String
		}

		resp.Sales = append(resp.Sales, &sale)
	}
	resp.Count = count
	return &resp, nil
}

func (s *saleRepo) Update(ctx context.Context, request *pbp.Sale) (*pbp.Sale, error) {
	var (
		params               = make(map[string]interface{})
		response             = pbp.Sale{}
		query                = `update sales set `
		filter               = ""
		createdAt, updatedAt = sql.NullString{}, sql.NullString{}
	)

	params["id"] = request.GetId()
	fmt.Println("courier id", request.GetId())

	if request.GetBarcode() != "" {
		params["barcode"] = request.GetBarcode()

		filter += " barcode = @barcode,"
	}

	if request.GetShiftId() != "" {
		params["shift_id"] = request.GetShiftId()

		filter += " shift_id = @shift_id,"
	}

	if request.GetSalePointId() != "" {
		params["sale_point_id"] = request.GetSalePointId()

		filter += " sale_point_id = @sale_point_id,"
	}

	if request.GetStatus() != "" {
		params["status"] = request.GetStatus()

		filter += " status = @status,"
	}

	if request.GetPayment() != "" {
		params["payment"] = request.GetPayment()

		filter += " payment = @payment,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, barcode, shift_id, sale_point_id, staff_id, status, payment, created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := s.db.QueryRow(ctx, fullQuery, args...).Scan(
		&response.Id,
		&response.Barcode,
		&response.ShiftId,
		&response.SalePointId,
		&response.StaffId,
		&response.Status,
		&response.Payment,
		&createdAt,
		&updatedAt,
	); err != nil {
		s.log.Error("error is while scanning updated fields from sale", logger.Error(err))
		return nil, err
	}

	if createdAt.Valid {
		response.CreatedAt = createdAt.String
	}

	if updatedAt.Valid {
		response.UpdatedAt = updatedAt.String
	}

	return &response, nil
}

func (s *saleRepo) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	query := `update sales set deleted_at = extract(epoch from current_timestamp) where id = $1`
	rowsAffected, err := s.db.Exec(ctx, query, key.GetId())
	if err != nil {
		s.log.Error("error is while deleting", logger.Error(err))
		return nil, err
	}

	if n := rowsAffected.RowsAffected(); n == 0 {
		s.log.Error("error is while rows affected", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}
