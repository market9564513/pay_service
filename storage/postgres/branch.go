package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbp "pay_service/genproto/pay_service"
	"pay_service/pkg/helper"
	"pay_service/pkg/logger"
)

type branchRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewBranchRepo(db *pgxpool.Pool, log logger.ILogger) *branchRepo {
	return &branchRepo{
		db:  db,
		log: log,
	}
}

func (b *branchRepo) Create(ctx context.Context, branch *pbp.Branch) (*pbp.Branch, error) {
	response := pbp.Branch{}
	searchingColumn := fmt.Sprintf("%s %s", branch.GetName(), branch.GetPhone())

	query := `insert into branches (id, name, address, phone, searching_column) values ($1, $2, $3, $4, $5)
                    returning id, name, address, phone, created_at::text`
	if err := b.db.QueryRow(ctx, query, uuid.New(), branch.GetName(), branch.GetAddress(), branch.GetPhone(), searchingColumn).Scan(
		&response.Id,
		&response.Name,
		&response.Address,
		&response.Phone,
		&response.CreatedAt,
	); err != nil {
		b.log.Error("error is while creating branch", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (b *branchRepo) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Branch, error) {
	response := pbp.Branch{}

	query := `select id, name, address, phone, created_at::text, updated_at::text from branches where id = $1 and deleted_at = 0`
	if err := b.db.QueryRow(ctx, query, key.GetId()).Scan(
		&response.Id,
		&response.Name,
		&response.Address,
		&response.Phone,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		b.log.Error("error is while getting branch", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (b *branchRepo) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.BranchesResponse, error) {
	var (
		branches []*pbp.Branch
		filter   = "where deleted_at = 0 "
		offset   = (request.GetPage() - 1) * request.GetLimit()
		count    = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%' ", request.GetSearch())
	}

	countQuery := `select count(1) from branches ` + filter

	if err := b.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		b.log.Error("error while scanning count of branches", logger.Error(err))
		return nil, err
	}

	query := `select id, name, address, phone, created_at::text, updated_at::text from branches ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := b.db.Query(ctx, query)
	if err != nil {
		b.log.Error("error while getting branch rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		branch := pbp.Branch{}

		if err = rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Address,
			&branch.Phone,
			&branch.CreatedAt,
			&branch.UpdatedAt,
		); err != nil {
			b.log.Error("error while scanning ony by one branch", logger.Error(err))
			return nil, err
		}

		branches = append(branches, &branch)
	}

	return &pbp.BranchesResponse{Branches: branches,
		Count: count}, nil
}

func (b *branchRepo) Update(ctx context.Context, branch *pbp.Branch) (*pbp.Branch, error) {
	var (
		resp   = pbp.Branch{}
		params = make(map[string]interface{})
		query  = `update branches set `
		filter = ""
	)

	params["id"] = branch.GetId()
	fmt.Println("branch id", branch.GetId())

	if branch.GetName() != "" {
		params["name"] = branch.GetName()

		filter += " name = @name,"
	}

	if branch.GetAddress() != "" {
		params["address"] = branch.GetAddress()

		filter += " address = @address,"
	}

	if branch.GetPhone() != "" {
		params["phone"] = branch.GetPhone()

		filter += " phone = @phone,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, name, address, phone, created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := b.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
		&resp.Name,
		&resp.Address,
		&resp.Phone,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (b *branchRepo) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	query := `update branches set deleted_at = extract(epoch from current_timestamp) where id = $1`
	if _, err := b.db.Exec(ctx, query, key.GetId()); err != nil {
		b.log.Error("error is while deleting branch", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}
