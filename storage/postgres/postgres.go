package postgres

import (
	"context"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/jackc/pgx/v5/pgxpool"
	"pay_service/config"
	"pay_service/pkg/logger"
	"pay_service/storage"
	"strings"

	_ "github.com/golang-migrate/migrate/v4/database"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

type Store struct {
	db  *pgxpool.Pool
	cfg config.Config
	log logger.ILogger
}

func New(ctx context.Context, cfg config.Config, log logger.ILogger) (storage.IStorage, error) {
	url := fmt.Sprintf(
		`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)

	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil {
		log.Error("error is while parsing config", logger.Error(err))
		return nil, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil {
		log.Error("error is while new with config", logger.Error(err))
		return nil, err
	}

	//migration
	m, err := migrate.New("file://migrations/postgres/", url)
	if err != nil {
		log.Error("error is while migrating", logger.Error(err))
		return nil, err
	}

	if err = m.Up(); err != nil {
		if !strings.Contains(err.Error(), "no change") {
			version, dirty, err := m.Version()
			if err != nil {
				log.Error("error is while migration up", logger.Any("version:", version), logger.Any("dirty", dirty), logger.Error(err))
				return nil, err
			}

			if dirty {
				version--
				if err = m.Force(int(version)); err != nil {
					log.Error("error is while force migrating", logger.Error(err))
					return nil, err
				}
			}
			log.Error("MIGRATING UP", logger.Error(err))
			return nil, err
		}
	}

	return &Store{
		db:  pool,
		cfg: cfg,
		log: log,
	}, nil
}

func (s *Store) Close() {
	s.db.Close()
}

func (s *Store) Branch() storage.IBranchStorage {
	return NewBranchRepo(s.db, s.log)
}

func (s *Store) SalePoint() storage.ISalePointStorage {
	return NewSalePointRepo(s.db, s.log)
}

func (s *Store) Staff() storage.IStaffStorage {
	return NewStaffService(s.db, s.log)
}

func (s *Store) Courier() storage.ICourierStorage {
	return NewCourierRepo(s.db, s.log)
}

func (s *Store) Shift() storage.IShiftStorage {
	return NewShiftRepo(s.db, s.log)
}

func (s *Store) Sale() storage.ISaleStorage {
	return NewSaleRepo(s.db, s.log)
}

func (s *Store) SaleProduct() storage.ISaleProductStorage {
	return NewSaleProductRepo(s.db, s.log)
}
