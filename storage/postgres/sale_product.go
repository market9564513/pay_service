package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbp "pay_service/genproto/pay_service"
	"pay_service/pkg/generate"
	"pay_service/pkg/helper"
	"pay_service/pkg/logger"
)

type saleProductRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewSaleProductRepo(db *pgxpool.Pool, log logger.ILogger) *saleProductRepo {
	return &saleProductRepo{
		db:  db,
		log: log,
	}
}

func (s *saleProductRepo) Create(ctx context.Context, request *pbp.CreateSaleProduct) (*pbp.SaleProduct, error) {
	response := pbp.SaleProduct{}
	barcode := generate.BarcodeGenerate()
	searchingColumn := fmt.Sprintf("%s %s", barcode, request.GetSaleId())

	query := `insert into sale_products (id, sale_id, product_id, barcode, quantity, exist_product_quantity, price, total_price, searching_column) 
                    values ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                    returning id, sale_id, product_id, barcode, quantity, exist_product_quantity, price, total_price, created_at::text`
	if err := s.db.QueryRow(ctx, query,
		uuid.New(),
		request.GetSaleId(),
		request.GetProductId(),
		barcode,
		request.GetQuantity(),
		request.GetExistProductQuantity(),
		request.GetPrice(),
		request.GetTotalPrice(),
		searchingColumn).
		Scan(
			&response.Id,
			&response.SaleId,
			&response.ProductId,
			&response.Barcode,
			&response.Quantity,
			&response.ExistProductQuantity,
			&response.Price,
			&response.TotalPrice,
			&response.CreatedAt,
		); err != nil {
		s.log.Error("error is while creating sale product", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *saleProductRepo) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.SaleProduct, error) {
	response := pbp.SaleProduct{}
	query := `select id, sale_id, product_id, barcode, quantity, exist_product_quantity, price, total_price, 
              created_at::text, updated_at::text from sale_products where id = $1 and deleted_at = 0`
	if err := s.db.QueryRow(ctx, query, key.GetId()).Scan(
		&response.Id,
		&response.SaleId,
		&response.ProductId,
		&response.Barcode,
		&response.Quantity,
		&response.ExistProductQuantity,
		&response.Price,
		&response.TotalPrice,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		s.log.Error("error is while getting by id", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *saleProductRepo) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.SaleProductsResponse, error) {
	var (
		offset                                = (request.GetPage() - 1) * request.GetLimit()
		resp                                  pbp.SaleProductsResponse
		filter, pagination, query, countQuery string
		count                                 = int32(0)
	)
	pagination = ` OFFSET $1 LIMIT $2`

	filter = `where deleted_at = 0`

	if request.GetSearch() != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%'", request.GetSearch())
	}

	countQuery = `select count(1) from sale_products ` + filter
	if err := s.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		s.log.Error("error is while scanning count", logger.Error(err))
		return nil, err
	}

	query = `select id, sale_id, product_id, barcode, quantity, exist_product_quantity, price, total_price, 
              created_at::text, updated_at::text from sale_products ` + filter + pagination
	rows, err := s.db.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		s.log.Error("error is while selecting all from sale product", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		saleProduct := pbp.SaleProduct{}
		if err := rows.Scan(
			&saleProduct.Id,
			&saleProduct.SaleId,
			&saleProduct.ProductId,
			&saleProduct.Barcode,
			&saleProduct.Quantity,
			&saleProduct.ExistProductQuantity,
			&saleProduct.Price,
			&saleProduct.TotalPrice,
			&saleProduct.CreatedAt,
			&saleProduct.UpdatedAt,
		); err != nil {
			s.log.Error("error is while scanning all in saleProduct", logger.Error(err))
			return nil, err
		}
		resp.SaleProducts = append(resp.SaleProducts, &saleProduct)
	}
	resp.Count = count
	return &resp, nil
}

func (s *saleProductRepo) Update(ctx context.Context, request *pbp.SaleProduct) (*pbp.SaleProduct, error) {
	var (
		params   = make(map[string]interface{})
		response = pbp.SaleProduct{}
		query    = `update sale_products set `
		filter   = ""
	)

	params["id"] = request.GetId()

	if request.GetBarcode() != "" {
		params["barcode"] = request.GetBarcode()

		filter += " barcode = @barcode,"
	}

	if request.GetSaleId() != "" {
		params["sale_id"] = request.GetSaleId()

		filter += " sale_id = @sale_id,"
	}

	if request.GetProductId() != "" {
		params["product_id"] = request.GetProductId()

		filter += " product_id = @product_id,"
	}

	if request.GetQuantity() != 0 {
		params["quantity"] = request.GetQuantity()

		filter += " quantity = @quantity,"
	}

	if request.GetExistProductQuantity() != 0 {
		params["exist_product_quantity"] = request.GetExistProductQuantity()

		filter += " exist_product_quantity = @exist_product_quantity,"
	}

	if request.GetPrice() != 0 {
		params["price"] = request.GetPrice()

		filter += " price = @price,"
	}

	if request.GetTotalPrice() != 0 {
		params["total_price"] = request.GetTotalPrice()

		filter += " total_price = @total_price,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, sale_id, product_id, barcode, quantity, exist_product_quantity, price, total_price, created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := s.db.QueryRow(ctx, fullQuery, args...).Scan(
		&response.Id,
		&response.SaleId,
		&response.ProductId,
		&response.Barcode,
		&response.Quantity,
		&response.ExistProductQuantity,
		&response.Price,
		&response.TotalPrice,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		s.log.Error("error is while scanning updated fields from sale product", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *saleProductRepo) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	query := `update sale_products set deleted_at = extract(epoch from current_timestamp) where id = $1`
	rowsAffected, err := s.db.Exec(ctx, query, key.GetId())
	if err != nil {
		s.log.Error("error is while deleting", logger.Error(err))
		return nil, err
	}

	if n := rowsAffected.RowsAffected(); n == 0 {
		s.log.Error("error is while rows affected", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}
