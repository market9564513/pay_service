package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbp "pay_service/genproto/pay_service"
	"pay_service/pkg/helper"
	"pay_service/pkg/logger"
)

type courierRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCourierRepo(db *pgxpool.Pool, log logger.ILogger) *courierRepo {
	return &courierRepo{
		db:  db,
		log: log,
	}
}

func (c *courierRepo) Create(ctx context.Context, courier *pbp.CreateCourier) (*pbp.Courier, error) {
	response := pbp.Courier{}
	searchingColumn := fmt.Sprintf("%s %s", courier.GetName(), courier.GetPhone())

	query := `insert into couriers (id, name, phone, active, searching_column) values ($1, $2, $3, $4, $5)
                    returning id, name, phone, active, created_at::text`
	if err := c.db.QueryRow(ctx, query,
		uuid.New(),
		courier.GetName(),
		courier.GetPhone(),
		courier.GetActive(),
		searchingColumn).
		Scan(
			&response.Id,
			&response.Name,
			&response.Phone,
			&response.Active,
			&response.CreatedAt,
		); err != nil {
		c.log.Error("error is while creating courier", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (c *courierRepo) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Courier, error) {
	response := pbp.Courier{}
	query := `select id, name, phone, active, created_at::text, updated_at::text from couriers where id = $1 and deleted_at = 0`
	if err := c.db.QueryRow(ctx, query, key.GetId()).Scan(
		&response.Id,
		&response.Name,
		&response.Phone,
		&response.Active,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		c.log.Error("error is while getting by id", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (c *courierRepo) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.CouriersResponse, error) {
	var (
		offset                                = (request.GetPage() - 1) * request.GetLimit()
		resp                                  pbp.CouriersResponse
		filter, pagination, query, countQuery string
		count                                 = int32(0)
	)
	pagination = ` OFFSET $1 LIMIT $2`

	filter = ` where deleted_at = 0 `

	if request.GetSearch() != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%'", request.GetSearch())
	}

	countQuery = `select count(1) from couriers ` + filter
	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error is while scanning count", logger.Error(err))
		return nil, err
	}

	query = `select id, name, phone, active, created_at::text, updated_at::text from couriers ` + filter + pagination
	rows, err := c.db.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		c.log.Error("error is while selecting all from couriers", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		courier := pbp.Courier{}
		if err := rows.Scan(
			&courier.Id,
			&courier.Name,
			&courier.Phone,
			&courier.Active,
			&courier.CreatedAt,
			&courier.UpdatedAt,
		); err != nil {
			c.log.Error("error is while scanning all in courier", logger.Error(err))
			return nil, err
		}
		resp.Couriers = append(resp.Couriers, &courier)
	}
	resp.Count = count
	return &resp, nil
}

func (c *courierRepo) Update(ctx context.Context, request *pbp.Courier) (*pbp.Courier, error) {
	var (
		params = make(map[string]interface{})
		resp   = pbp.Courier{}
		query  = `update couriers set `
		filter = ""
	)

	params["id"] = request.GetId()
	fmt.Println("courier id", request.GetId())

	if request.GetName() != "" {
		params["name"] = request.GetName()

		filter += " name = @name,"
	}

	if request.GetPhone() != "" {
		params["phone"] = request.GetPhone()

		filter += " phone = @phone,"
	}

	if request.GetActive() {
		params["active"] = request.GetActive()

		filter += " active = @active,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, name, phone, created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := c.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
		&resp.Name,
		&resp.Phone,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		c.log.Error("error is while scanning updated fields from courier", logger.Error(err))
		return nil, err
	}

	return &resp, nil
}

func (c *courierRepo) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	query := `update couriers set deleted_at = extract(epoch from current_timestamp) where id = $1`
	rowsAffected, err := c.db.Exec(ctx, query, key.GetId())
	if err != nil {
		c.log.Error("error is while deleting", logger.Error(err))
		return nil, err
	}

	if n := rowsAffected.RowsAffected(); n == 0 {
		c.log.Error("error is while rows affected", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}
