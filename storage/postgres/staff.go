package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbp "pay_service/genproto/pay_service"
	"pay_service/pkg/helper"
	"pay_service/pkg/logger"
	"pay_service/pkg/security"
)

type staffRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	pbp.UnimplementedStaffServiceServer
}

func NewStaffService(db *pgxpool.Pool, log logger.ILogger) *staffRepo {
	return &staffRepo{
		db:  db,
		log: log,
	}
}

func (s *staffRepo) Create(ctx context.Context, request *pbp.CreateStaff) (*pbp.Staff, error) {
	response := pbp.Staff{}
	hashedPassword, err := security.HashPassword(request.Password)
	if err != nil {
		s.log.Error("error is while hashing password", logger.Error(err))
		return nil, err
	}
	searchingColumn := fmt.Sprintf("%s %s %s %s", request.GetFirstName(), request.GetLastName(), request.GetPhone(), request.GetSalesPointId())

	query := `insert into staffs (id, first_name, last_name, phone, login, password, sales_point_id, user_role, searching_column) 
                    values ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                    returning id, first_name, last_name, phone, login, password, sales_point_id, user_role, created_at::text`
	if err := s.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.GetFirstName(),
		request.GetLastName(),
		request.GetPhone(),
		request.GetLogin(),
		hashedPassword,
		request.GetSalesPointId(),
		request.GetUserRole(),
		searchingColumn).
		Scan(
			&response.Id,
			&response.FirstName,
			&response.LastName,
			&response.Phone,
			&response.Login,
			&response.Password,
			&response.SalesPointId,
			&response.UserRole,
			&response.CreatedAt,
		); err != nil {
		s.log.Error("error is while creating sale", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *staffRepo) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.Staff, error) {
	response := pbp.Staff{}
	query := `select id, first_name, last_name, phone, login, password, sales_point_id, user_role, created_at::text, updated_at::text from staffs where id = $1 and deleted_at = 0`
	if err := s.db.QueryRow(ctx, query, key.GetId()).Scan(
		&response.Id,
		&response.FirstName,
		&response.LastName,
		&response.Phone,
		&response.Login,
		&response.Password,
		&response.SalesPointId,
		&response.UserRole,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		s.log.Error("error is while getting by id", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *staffRepo) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.StaffsResponse, error) {
	var (
		offset                                = (request.GetPage() - 1) * request.GetLimit()
		resp                                  pbp.StaffsResponse
		filter, pagination, query, countQuery string
		count                                 = int32(0)
	)
	pagination = ` OFFSET $1 LIMIT $2`

	filter = ``

	if request.GetSearch() != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%", request.GetSearch())
	}

	countQuery = `select count(1) from staffs ` + filter
	if err := s.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		s.log.Error("error is while scanning count", logger.Error(err))
		return nil, err
	}

	query = `select id, first_name, last_name, phone, login, password, sales_point_id, user_role, 
             created_at::text, updated_at::text from staffs where deleted_at = 0 ` + filter + pagination
	rows, err := s.db.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		s.log.Error("error is while selecting all from staffs", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		staff := pbp.Staff{}
		if err := rows.Scan(
			&staff.Id,
			&staff.FirstName,
			&staff.LastName,
			&staff.Phone,
			&staff.Login,
			&staff.Password,
			&staff.SalesPointId,
			&staff.UserRole,
			&staff.CreatedAt,
			&staff.UpdatedAt,
		); err != nil {
			s.log.Error("error is while scanning all in staff", logger.Error(err))
			return nil, err
		}
		resp.Staffs = append(resp.Staffs, &staff)
	}
	resp.Count = count
	return &resp, nil
}

func (s *staffRepo) Update(ctx context.Context, request *pbp.Staff) (*pbp.Staff, error) {
	var (
		params   = make(map[string]interface{})
		response = pbp.Staff{}
		query    = `update staffs set `
		filter   = ""
	)

	params["id"] = request.GetId()
	fmt.Println("courier id", request.GetId())

	if request.GetFirstName() != "" {
		params["first_name"] = request.GetFirstName()

		filter += " first_name = @first_name,"
	}

	if request.GetLastName() != "" {
		params["last_name"] = request.GetLastName()

		filter += " last_name = @last_name,"
	}

	if request.GetPhone() != "" {
		params["phone"] = request.GetPhone()

		filter += " phone = @phone,"
	}

	if request.GetLogin() != "" {
		params["login"] = request.GetLogin()

		filter += " login = @login,"
	}

	if request.GetPassword() != "" {
		params["password"] = request.GetPassword()

		filter += " password = @password,"
	}

	if request.GetSalesPointId() != "" {
		params["sales_point_id"] = request.GetSalesPointId()

		filter += " sales_point_id = @sales_point_id,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, first_name, last_name, phone, login, password, sales_point_id, user_role, created_at::text, updated_at::text`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := s.db.QueryRow(ctx, fullQuery, args...).Scan(
		&response.Id,
		&response.FirstName,
		&response.LastName,
		&response.Phone,
		&response.Login,
		&response.Password,
		&response.SalesPointId,
		&response.UserRole,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		s.log.Error("error is while scanning updated fields from staff", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *staffRepo) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	query := `update staffs set deleted_at = extract(epoch from current_timestamp) where id = $1`
	rowsAffected, err := s.db.Exec(ctx, query, key.GetId())
	if err != nil {
		s.log.Error("error is while deleting", logger.Error(err))
		return nil, err
	}

	if n := rowsAffected.RowsAffected(); n == 0 {
		s.log.Error("error is while rows affected", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *staffRepo) GetByCredentials(ctx context.Context, request *pbp.LoginRequest) (*pbp.Staff, error) {
	staff := pbp.Staff{}
	query := `select user_role, phone, password from staffs where login = $1`
	if err := s.db.QueryRow(ctx, query, request.GetLogin()).
		Scan(
			&staff.UserRole,
			&staff.Phone,
			&staff.Password,
		); err != nil {
		s.log.Error("error is while getting credentials", logger.Error(err))
		return nil, err
	}

	return &staff, nil
}
