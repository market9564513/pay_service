package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jackc/pgx/v5/pgxpool"
	pbp "pay_service/genproto/pay_service"
	"pay_service/pkg/generate"
	"pay_service/pkg/helper"
	"pay_service/pkg/logger"
)

type salePointRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewSalePointRepo(db *pgxpool.Pool, log logger.ILogger) *salePointRepo {
	return &salePointRepo{
		db:  db,
		log: log,
	}
}

func (s *salePointRepo) Create(ctx context.Context, request *pbp.CreateSalesPoint) (*pbp.SalesPoint, error) {
	var (
		response   = pbp.SalesPoint{}
		saleID, id string
	)

	searchingColumn := fmt.Sprintf("%s", request.GetName())

	saleIDQuery := `select id from sales_points order by id desc limit 1`
	if err := s.db.QueryRow(ctx, saleIDQuery).Scan(&saleID); err != nil {
		id = generate.PointIDGenerate(request.Name, "")
	} else {
		id = generate.PointIDGenerate(request.Name, saleID)
	}
	fmt.Println("id", id)

	query := `insert into sales_points(id, branch_id, name, income_id, searching_column) 
                    values ($1, $2, $3, $4, $5)
                    returning id, branch_id, name, income_id, created_at::text`
	if err := s.db.QueryRow(ctx, query,
		id,
		request.GetBranchId(),
		request.GetName(),
		request.GetIncomeId(),
		searchingColumn).
		Scan(
			&response.Id,
			&response.BranchId,
			&response.Name,
			&response.IncomeId,
			&response.CreatedAt,
		); err != nil {
		s.log.Error("error is while creating sale", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *salePointRepo) Get(ctx context.Context, key *pbp.PrimaryKey) (*pbp.SalesPoint, error) {
	response := pbp.SalesPoint{}
	query := `select id, branch_id, name, income_id, created_at::text, updated_at::text from sales_points where id = $1 and deleted_at = 0`
	if err := s.db.QueryRow(ctx, query, key.GetId()).Scan(
		&response.Id,
		&response.BranchId,
		&response.Name,
		&response.IncomeId,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		s.log.Error("error is while getting by id", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *salePointRepo) GetList(ctx context.Context, request *pbp.GetRequest) (*pbp.SalesPointsResponse, error) {
	var (
		offset                                = (request.GetPage() - 1) * request.GetLimit()
		resp                                  pbp.SalesPointsResponse
		filter, pagination, query, countQuery string
		count                                 = int32(0)
	)
	pagination = ` OFFSET $1 LIMIT $2`

	filter = ` where deleted_at = 0 `

	if request.GetSearch() != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%%%s%%'", request.GetSearch())
	}

	countQuery = `select count(1) from sales_points ` + filter
	if err := s.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		s.log.Error("error is while scanning count", logger.Error(err))
		return nil, err
	}

	query = `select id, branch_id, name, income_id, created_at::text, updated_at::text from sales_points ` + filter + pagination
	rows, err := s.db.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		s.log.Error("error is while selecting all from sales", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		salesPoint := pbp.SalesPoint{}
		if err := rows.Scan(
			&salesPoint.Id,
			&salesPoint.BranchId,
			&salesPoint.Name,
			&salesPoint.IncomeId,
			&salesPoint.CreatedAt,
			&salesPoint.UpdatedAt,
		); err != nil {
			s.log.Error("error is while scanning all in sales point", logger.Error(err))
			return nil, err
		}
		resp.SalesPoints = append(resp.SalesPoints, &salesPoint)
	}

	resp.Count = count
	return &resp, nil
}

func (s *salePointRepo) Update(ctx context.Context, request *pbp.SalesPoint) (*pbp.SalesPoint, error) {
	var (
		params   = make(map[string]interface{})
		response = pbp.SalesPoint{}
		query    = `update sales_points set `
		filter   = ""
	)

	params["id"] = request.GetId()
	fmt.Println("courier id", request.GetId())

	if request.GetBranchId() != "" {
		params["branch_id"] = request.GetBranchId()

		filter += " branch_id = @branch_id,"
	}

	if request.GetName() != "" {
		params["name"] = request.GetName()

		filter += " name = @name,"
	}

	if request.GetIncomeId() != "" {
		params["income_id"] = request.GetIncomeId()

		filter += " income_id = @income_id,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id, branch_id, name, income_id, created_at::text, updated_at::text `

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := s.db.QueryRow(ctx, fullQuery, args...).Scan(
		&response.Id,
		&response.BranchId,
		&response.Name,
		&response.IncomeId,
		&response.CreatedAt,
		&response.UpdatedAt,
	); err != nil {
		s.log.Error("error is while scanning updated fields from sales point", logger.Error(err))
		return nil, err
	}

	return &response, nil
}

func (s *salePointRepo) Delete(ctx context.Context, key *pbp.PrimaryKey) (*empty.Empty, error) {
	query := `update sales_points set deleted_at = extract(epoch from current_timestamp) where id = $1`
	rowsAffected, err := s.db.Exec(ctx, query, key.GetId())
	if err != nil {
		s.log.Error("error is while deleting", logger.Error(err))
		return nil, err
	}

	if n := rowsAffected.RowsAffected(); n == 0 {
		s.log.Error("error is while rows affected", logger.Error(err))
		return nil, err
	}

	return &empty.Empty{}, nil
}
